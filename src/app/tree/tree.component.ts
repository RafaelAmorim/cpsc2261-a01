import { Component, OnInit } from '@angular/core';

export default class Tree {
  growth: number;
  fruitTaken: number;

  constructor(initialGrowth: number = 0, initialFruitTaken: number = 0) {
    this.growth = initialGrowth;
    this.fruitTaken = initialFruitTaken;
  }

  grow(growthScale: number = 1): void {
    this.growth += growthScale;
  }

  fruitAvailable(): number {
    if (this.growth > 0) {
      return Math.floor(Math.log10(this.growth) - this.fruitTaken);
    }
    return 0;
  }

  size(): number {
    if (this.growth > 0) {
      return Math.pow(this.growth, 2 / 3);
    }
    return 0;
  }
}
@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {
  tree = new Tree();
  ngOnInit(): void { }
}
