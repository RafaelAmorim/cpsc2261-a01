import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeComponent } from './tree.component';

describe('TreeComponent', () => {
  let component: TreeComponent;
  let fixture: ComponentFixture<TreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TreeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // TESTING TREE
  it('should create an instance of a tree', () => {
    expect(component.tree).toBeTruthy();
  });

  it('should create a tree with growth equal to 0', () => {
    expect(component.tree.growth).toEqual(0);
  });

  it('should create a tree with fruitTaken equal to 0', () => {
    expect(component.tree.fruitTaken).toEqual(0);
  });

  // TESTING FRUIT AVAILABLE METHOD
  it('should return 0 fruits availabe for a new tree', () => {
    expect(component.tree.fruitAvailable()).toEqual(0);
  });

  it('should return 0 fruits availabe for a tree with negative growth', () => {
    component.tree.growth = -20;
    expect(component.tree.fruitAvailable()).toEqual(0);
  });

  it('should return 0 fruits availabe for a tree with positive growth', () => {
    component.tree.growth = 9;
    expect(component.tree.fruitAvailable()).toEqual(0);
  });

  it('should return 1 fruit availabe for a tree with growth equals 10', () => {
    component.tree.growth = 10;
    expect(component.tree.fruitAvailable()).toEqual(1);
  });

  it('should return 0 fruitS availabe for a tree with 10 growth and 1 fruit taken ', () => {
    component.tree.growth = 10;
    component.tree.fruitTaken = 1;
    expect(component.tree.fruitAvailable()).toEqual(0);
  });

  // TESTING SIZE METHOD
  it('should return 0 size for a new tree', () => {
    expect(component.tree.size()).toEqual(0);
  });

  it('should return 0 size for a tree with negative growth', () => {
    component.tree.growth = -10;
    expect(component.tree.size()).toEqual(0);
  });

  it('should return 1 size for a tree with 1 growth', () => {
    component.tree.growth = 1;
    expect(component.tree.size()).toEqual(1);
  });

  it('should return the tree size for a tree with > 1 growth', () => {
    component.tree.growth = 50;
    expect(component.tree.size()).toBeGreaterThan(1);
  });
});
