import { Component, OnInit } from '@angular/core';
import Tree from '../tree/tree.component';

class Orchard {
  size: number;
  trees: Tree[];

  constructor(size: number) {
    this.size = size;
    this.trees = [];
  }

  addTree(t: Tree): void {
    if (this.canFit(t)) {
      this.trees.push(t);
    }
  }

  totalFruitAvailable(): number {
    let totalFruits = 0;

    this.trees.map(tree => {
      totalFruits += tree.fruitAvailable();
    });

    return totalFruits;
  }

  canFit(t: Tree): boolean {
    let currentSpaceTaken = 0;

    this.trees.map(tree => {
      currentSpaceTaken += tree.size();
    });

    const availableSpace = this.size - currentSpaceTaken - t.size();

    return (availableSpace < 0) ? false : true;
  }
}

@Component({
  selector: 'app-orchard',
  templateUrl: './orchard.component.html',
  styleUrls: ['./orchard.component.scss']
})
export class OrchardComponent implements OnInit {
  orchard = new Orchard(50);
  ngOnInit(): void { }
}
