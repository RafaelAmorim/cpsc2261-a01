import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrchardComponent } from './orchard.component';
import Tree from '../tree/tree.component';

describe('OrchardComponent', () => {
  let component: OrchardComponent;
  let fixture: ComponentFixture<OrchardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrchardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // TESTING ORCHARD
  it('should create an instance of an orchard', () => {
    expect(component.orchard).toBeTruthy();
  });

  it('should create an orchard with size 50', () => {
    expect(component.orchard.size).toEqual(50);
  });

  it('should create an orchard with no trees', () => {
    expect(component.orchard.trees).toEqual([]);
  });

  // TESTING CAN FIT METHOD
  it('should accept a tree that fits on a new orchard', () => {
    const tree1 = new Tree();
    expect(component.orchard.canFit(tree1)).toBeTruthy();
  });

  it('should not accept a tree that does not fit on a new orchard', () => {
    const tree1 = new Tree(500, 0);
    expect(component.orchard.canFit(tree1)).toBeFalsy();
  });

  // TESTING TOTAL FRUIT AVAILABLE METHOD
  it('should return 0 total fruits available for an orchard with one tree without fruits', () => {
    const tree1 = new Tree();
    component.orchard.addTree(tree1);
    expect(component.orchard.totalFruitAvailable()).toEqual(0);
  });

  it('should return 1 total fruits available for an orchard with one tree with 1 fruit', () => {
    const tree1 = new Tree(10);
    component.orchard.addTree(tree1);
    expect(component.orchard.totalFruitAvailable()).toEqual(1);
  });

  it('should return 2 total fruits available for an orchard with two trees with 1 fruit each', () => {
    const tree1 = new Tree(10);
    const tree2 = new Tree(10);
    component.orchard.addTree(tree1);
    component.orchard.addTree(tree2);
    expect(component.orchard.totalFruitAvailable()).toEqual(2);
  });
});
